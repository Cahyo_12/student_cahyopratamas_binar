package challenge;

import java.util.ArrayList;
import java.util.Scanner;

public class Nilai {

	private ArrayList<Integer> angka;
		// TODO Auto-generated method stub
	Nilai(ArrayList<Integer> angka) {
        this.angka = angka;
    }
	
	public ArrayList<Integer> getNilai() {
        return angka;
    }

    public void setNilai(ArrayList<Integer> angka) {
        this.angka = angka;
    }
    public float getmean() {
    	float mean = 0;
    	for(int i = 1; i < angka.size(); i ++) {
    		mean += angka.get(i);
    	}
    	return mean/angka.size();
    }
    public float getmedian() {
    	float median = 0;
    	if (angka.size() % 2 == 0) {
    		median = (angka.get(angka.size() / 2) + angka.get(angka.size() / 2 - 1)) / 2;
    	}else {
    		median = angka.get(angka.size() / 2);
    	}
    	return median;
    }
    public int getfrekuensi(int min, int max) {
    	int count = 0;
    	for(int i = 0; i < angka.size(); i++) {
    		if (angka.get(i) >= min && angka.get(i) <= max){
    			count++;
    		}
    	}
    	return count;
    }
    public int getmodus() {
    	int modus = 0;
    	int JT = 0;
    	for (int n : angka) {
    		int frekuensi = getfrekuensi(n,n);
    		if(frekuensi > JT) {
    			modus = n;
    			JT = frekuensi;
    		}
    	}
    	return modus;
    }
    public boolean getMenu() {
		// TODO Auto-generated method stub
		      while(true) {
		      String m;
		      
		      System.out.println("--------------------------------------");
		      System.out.println("Aplikasi Pengolah Nilai Siswa"  );
		      System.out.println("--------------------------------------");
		      System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut: C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\data_sekolah.csv");
		      System.out.println();
		      System.out.println("Pilih Menu");
		      System.out.println("1. Generete txt untuk menampilkan modus");
		      System.out.println("2. Generete txt untuk menampilkan nilai rata-rata, median");
		      System.out.println("3. Generete kedua file");
		      System.out.println("0. Exit");
		      System.out.println("--------------------------------------");
		      
		      Scanner input = new Scanner(System.in);
		      System.out.print("Masukan Pilih yang mana: ");
		      String pilih = input.nextLine();
		      switch(pilih) {
		      case "1" :
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("Aplikasi Pengolah Nilai Siswa");
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("File telah di generete di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\data_sekolah.csv");
		    	  System.out.println("Silahkan cek");
		    	  System.out.println();
		    	  System.out.println("0. Exit");
		    	  System.out.println("1. Kembali ke Menu Utama");
		    	  m = input.nextLine();
		    	  switch(m){
		    	  case "1" :
		    		  continue;
		    	  case "0" :
		    		  System.exit(0);
		    	  }
		      case "2" :
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("Aplikasi Pengolah Nilai Siswa");
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("File telah di generete di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\data_sekolah.csv");
		    	  System.out.println("Silahkan cek");
		    	  System.out.println();
		    	  System.out.println("0. Exit");
		    	  System.out.println("1. Kembali ke Menu Utama");
		    	  m = input.nextLine();
		    	  switch(m){
		    	  case "1" :
		    		  continue;
		    	  case "0" :
		    		  System.exit(0);
		      }
		      case "3" :
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("Aplikasi Pengolah Nilai Siswa");
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("File telah di generete di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\data_sekolah.csv");
		    	  System.out.println("Silahkan cek");
		    	  System.out.println();
		    	  System.out.println("0. Exit");
		    	  System.out.println("1. Kembali ke Menu Utama");
		    	  m = input.nextLine();
		    	  switch(m){
		    	  case "1" :
		    		  continue;
		    	  case "0" :
		    		  System.out.println("Terima Kasih Banyak..!!!!");
		    		  System.exit(0);
		    	  }
		      case "4" :
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("Aplikasi Pengolah Nilai Siswa");
		    	  System.out.println("-----------------------------------");
		    	  System.out.println("File tidak ditemukan");
		    	  System.out.println();
		    	  System.out.println("0. Exit");
		    	  System.out.println("1. Kembali ke Menu Utama");
		    	  m = input.nextLine();
		    	  switch(m){
		    	  case "1" :
		    		  continue;
		    	  case "0" :
		    		  System.out.println("Terima Kasih Banyak..!!!!");
		    		  System.exit(0);
		      }
		    	  input.close();
		      }
		      }
	}
}
