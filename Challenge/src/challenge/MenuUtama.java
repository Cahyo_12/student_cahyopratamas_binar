package challenge;

import java.util.Scanner;

public class MenuUtama {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("WELCOME");
		boolean i = true;
	      while(true) {
	      String memilih;
	      
	      System.out.println("--------------------------------------");
	      System.out.println("Kalkulator Penghitung Luas dan Volum"  );
	      System.out.println("--------------------------------------");
	      System.out.println("Menu"                                  );
	      System.out.println("1. Hitung Luas Bidang"                 );
	      System.out.println("2. Hitung Volume"                      );
	      System.out.println("0. Tutup Aplikasi"                     );
	      System.out.println("--------------------------------------");
	      
	      Scanner input = new Scanner(System.in);
	      System.out.print("Masukan Pilih yang mana: ");
	      String pilih = input.nextLine();
	      switch(pilih) {
	      case "1" : 
	      System.out.println("--------------------------------------");
	      System.out.println("Pilih bidang yang akan dihitung"       );
	         System.out.println("-----------------------------------");
	         System.out.println("1. Persegi"                         );
	         System.out.println("2. Lingkaran"                       );
	         System.out.println("3. Segitiga"                        );
	         System.out.println("4. Persegi Panjang"                 );
	         System.out.println("0. Kembali ke menu sebelumnya"      );
	         System.out.println("-----------------------------------");
	         memilih = input.nextLine();
	           switch(memilih) {
	           case "1" : 
	            System.out.println("------------------------------------------");
	            System.out.println("Anda memilih persegi"                      );
	            System.out.println("------------------------------------------");
	            System.out.print("Masukkan sisi : ");
	            int sisi = input.nextInt();
	            System.out.println("");
	            System.out.println("processing..............");
	            System.out.println("");
	            System.out.println("luas dari persegi adalah = " + sisi * sisi) ;
	            System.out.println("------------------------------------------");
	            System.out.println("tekan apa saja untuk kembali ke menu utama");
	            continue;
	           case "2" : 
	            System.out.println("------------------------------------------");
	            System.out.println("Anda memilih lingkaran"                    );
	            System.out.println("------------------------------------------");
	            System.out.print("Masukkan jari-jari : ");
	            int jari = input.nextInt();
	            System.out.println("");
	            System.out.println("processing.............");
	            System.out.println("");
	            System.out.println("luas dari lingkaran adalah = " + 3.14 * jari * jari) ;
	            System.out.println("------------------------------------------");
	            System.out.println("tekan apa saja untuk kembali ke menu utama");
	            continue;
	           case "3" : 
	            System.out.println("------------------------------------------");
	            System.out.println("Anda memilih segitiga"                     );
	            System.out.println("------------------------------------------");
	            System.out.print("Masukkan alas : "                            );
	            int alas = input.nextInt();
	            System.out.println("");
	            System.out.print("Masukkan tinggi : ");
	            int tinggi = input.nextInt();
	            System.out.println("");
	            System.out.println("processing............");
	            System.out.println("");
	            System.out.println("luas dari segitiga adalah = " + (alas * tinggi) / 2) ;
	            System.out.println("------------------------------------------");
	            System.out.println("tekan apa saja untuk kembali ke menu utama");
	            
	            continue;
	           case "4" : 
	            System.out.println("------------------------------------------");
	            System.out.println("Anda memilih persegi panjang"              );
	            System.out.println("------------------------------------------");
	            System.out.print("Masukkan panjang : ");
	            int p = input.nextInt();
	            System.out.println("");
	            System.out.print("Masukkan lebar : ");
	            int l = input.nextInt();
	            System.out.println("");
	            System.out.println("processing.........");
	            System.out.println("");
	            System.out.println("luas dari persegi panjang adalah = " + p * l) ;
	            System.out.println("------------------------------------------");
	            System.out.println("tekan apa saja untuk kembali ke menu utama");
	            continue;
	           case "0" :
	             continue;
	           }
	          case "2" : 
	          System.out.println("--------------------------------------");
	          System.out.println("Pilih bidang yang akan dihitung"       );
	          System.out.println("--------------------------------------");
	          System.out.println("1. Kubus"                              );
	          System.out.println("2. Balok"                              );
	          System.out.println("3. Tabung"                             );
	          System.out.println("0. Kembali ke menu sebelumnya"         );
	          System.out.println("--------------------------------------"); 
	           memilih = input.nextLine();
	            switch(memilih) {
	            case "1" :
	             System.out.println("------------------------------------------");
	             System.out.println("kubus"                                     );
	             System.out.println("------------------------------------------");
	             System.out.print("Masukkan sisi : ");
	             int s = input.nextInt();
	             System.out.println("");
	             System.out.println("processing.........");
	             System.out.println("");
	             System.out.println("volume dari kubus adalah = " + s * s * s) ;
	             System.out.println("------------------------------------------");
	             System.out.println("tekan apa saja untuk kembali ke menu utama");
	             continue;
	            case "2" :
	             System.out.println("------------------------------------------");
	             System.out.println("balok"                                     );
	             System.out.println("------------------------------------------");
	             System.out.print("Masukkan panjang : ");
	             int panjang = input.nextInt();
	             System.out.println("");
	             System.out.print("Masukkan lebar : ");
	             int lebar = input.nextInt();
	             System.out.println("");
	             System.out.print("Masukkan tinggi : ");
	             int tinggi2 = input.nextInt();
	             System.out.println("");
	             System.out.println("processing..........");
	             System.out.println("");
	             System.out.println("volume dari balok adalah = " + panjang * lebar * tinggi2) ;
	             System.out.println("------------------------------------------");
	             System.out.println("tekan apa saja untuk kembali ke menu utama");
	             continue;
	            case "3" :
	             System.out.println("------------------------------------------");
	             System.out.println("tabung");
	             System.out.println("------------------------------------------");
	             System.out.print("Masukkan tinggi : ");
	             int t = input.nextInt();
	             System.out.println("");
	             System.out.print("Masukkan jari-jari : ");
	             int j = input.nextInt();
	             System.out.println("");
	             System.out.println("processing"                                );
	             System.out.println("");
	             System.out.println("volume dari tabung adalah = " + 3.14 * t * (j)) ;
	             System.out.println("------------------------------------------");
	             System.out.println("tekan apa saja untuk kembali ke menu utama");
	             continue;
	            case "0" :
	            continue;
	            }
	            case "0" :
	            	System.out.println("Terima Kasih Banyak Yaaa.....!!!");
	            System.exit(0);
	            }
	      }
	  }
}