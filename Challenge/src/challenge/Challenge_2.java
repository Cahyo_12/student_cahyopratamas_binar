package challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.io.*;

public class Challenge_2 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ArrayList<Integer> DataNilai = read("C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\data_sekolah.csv");
		Collections.sort(DataNilai);
		Nilai nilai = new Nilai(DataNilai);
		Data(nilai);
		frekuensi(nilai);
		System.out.println(nilai.getMenu());
	}
	public static ArrayList<Integer> read(String csvFile) {
		ArrayList<Integer> dataNilai = new ArrayList();
		try {
			File file = new File(csvFile);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			String[] tempArr;
			while ((line = br.readLine()) != null) {
				tempArr = line.split(";");
				for(int i = 1; i < tempArr.length - 1; i++) {
					dataNilai.add(Integer.valueOf(tempArr[i]));
				}
			}
			br.close();
			System.out.println();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return dataNilai;
	}
	public static void Data(Nilai nilai)throws IOException {
		String path = "C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\Data_Nilai.txt";
		File file = new File(path);
		BufferedWriter tulis = new BufferedWriter(new FileWriter(file));
		
		tulis.write("Berikut Hasil Pengolahan Nilai: ");
		tulis.newLine();
		tulis.write("Rata - rata: " + nilai.getmean());
		tulis.newLine();
		tulis.write("Median: " + nilai.getmedian());
		tulis.newLine();
		tulis.write("Modus: " + nilai.getmodus());
		tulis.newLine();
		
		tulis.close();
		
		System.out.println("Berhasil Menulis di Sebuah File");
	}
	public static void frekuensi(Nilai nilai) throws IOException {
		String path = "C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\bin\\challenge\\Data_Frekuensi.txt";
		File file = new File(path);
		BufferedWriter tulis = new BufferedWriter(new FileWriter(file));
		
		tulis.write("Berikut Hasil Pengolahan Nilai: ");
		tulis.newLine();
		tulis.write(" ");
		tulis.newLine();
		tulis.write("\t Nilai \t\t |   frekuensi  \t ");
		tulis.newLine();
		tulis.write("    Kurang dari 6 \t | \t " + nilai.getfrekuensi(0, 5));
		tulis.newLine();
		tulis.write("\t   6 \t\t | \t " + nilai.getfrekuensi(6, 6));
		tulis.newLine();
		tulis.write("\t   7 \t\t | \t " + nilai.getfrekuensi(7, 7));
		tulis.newLine();
		tulis.write("\t   8 \t\t | \t " + nilai.getfrekuensi(8, 8));
		tulis.newLine();
		tulis.write("\t   9 \t\t | \t " + nilai.getfrekuensi(9, 9));
		tulis.newLine();
		tulis.write("\t   10 \t\t | \t " + nilai.getfrekuensi(10, 10));
		tulis.newLine();
		
		tulis.close();
		
		System.out.println("Berhasil Menulis di Sebuah File");
	}
}
