package jumat_2;

import java.util.ArrayList;

public class AtmCahyo implements ATM{

	private int saldo;
	private ArrayList<mutasirekening>mutasi;
	AtmCahyo(int balance){
		this.saldo = balance;
		this.mutasi = new ArrayList<>();
	}
	@Override
	public int getsaldo() {
		return this.saldo;
	}
	@Override
	public void penarikan(int nominal) {
		if(nominal <= this.saldo) {
			this.saldo -= nominal;
			mutasi.add(new mutasirekening ("penarikan ", nominal));
			System.out.println("Transaksi Berhasil");
		}else 
			System.out.println("Transaksi Gagal, Saldo Tidak Mencukupi");
	}
	@Override
	public void tambahansaldo(int nominal) {
		// TODO Auto-generated method stub
		this.saldo += nominal;
		mutasi.add(new mutasirekening ("penarikan ", nominal));
		System.out.println("Transaksi Berhasil");
	}
	@Override
	public void transfer(int nominal, String penerima) {
		if(nominal <= this.saldo) {
			this.saldo -= nominal;
			mutasi.add(new mutasirekening ("Transfer " + penerima, nominal));
			System.out.println("Transaksi Berhasil");
		}else 
			System.out.println("Transaksi Gagal, Saldo Tidak Mencukupi");
	}
	@Override
	public void printmutasirekening() {
		for(mutasirekening history: mutasi) {
			System.out.println("Transaksi:\t\t|\t\tNominal");
			System.out.println(history.gettransaksi() + "\t \t | \t \t " + history.getnominal());

		}
	}

}
