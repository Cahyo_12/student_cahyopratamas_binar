package jumat_2;

import java.util.Scanner;

public class Ujicoba {
	public static void main(String[] args) {
		AtmCahyo atm = new AtmCahyo(0);
		menu(atm);
    }
	public static void menu(AtmCahyo atm) {
		Scanner m = new Scanner(System.in);
		  System.out.println("============================================================");
		  System.out.println("                       Aplikasi ATM                         ");
		  System.out.println("============================================================");
		  System.out.println(" Pilih Menu :                                               ");
		  System.out.println("1. Lihat Saldo                                              ");
		  System.out.println("2. Ambil Saldo                                              ");
		  System.out.println("3. Tambah Saldo                                             ");
		  System.out.println("4. Tranfer                                                  ");
		  System.out.println("0. Exit                                                     ");
		  System.out.println("                                                            ");
		  System.out.print("Masukkan Pilihan Anda : ");
		  
		 byte input1 = m.nextByte();
	        int Nominal;


	        switch (input1) {
	            case 1:
	                System.out.println("Saldo anda berjumlah " + atm.getsaldo());
	                menu(atm);
	                break;
	            case 2:
	                System.out.print("Masukan jumlah saldo yang akan ditarik: ");
	                Nominal = m.nextInt();
	                atm.penarikan(Nominal);
	                menu(atm);
	                break;
	            case 3:
	                System.out.print("Masukan jumlah penambahan saldo ");
	                Nominal = m.nextInt();
	                atm.tambahansaldo(Nominal);
	                menu(atm);
	                break;
	            case 4:
	            	System.out.print("Masukan jumlah saldo yang akan ditransfer ");
	                Nominal = m.nextInt();
	                System.out.print("Masukan nomor rekening tujuan ");
	                String receiver = m.next();
	                atm.transfer(Nominal, receiver);
	                menu(atm);
	                break;
	            case 5:
	            	atm.printmutasirekening();
	                menu(atm);
	            case 6:
	            	System.exit(0);
	        }
	        m.close();
	}
}
