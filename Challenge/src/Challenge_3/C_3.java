package Challenge_3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import Challenge_3.Nilai_C3;

public class C_3 {

	public static void main(String[] data_sekolah) throws IOException {
		// TODO Auto-generated method stub
		ArrayList<Integer> DataNilai = read ("C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_sekolah.csv");
		Collections.sort(DataNilai);
		Nilai_C3 nilai = new Nilai_C3(DataNilai);
		menu(nilai);
		}
		
		public static ArrayList<Integer> read(String datasekolah) {
			ArrayList<Integer> dataNilai = new ArrayList<>();
			try {
				File file = new File (datasekolah);
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String line = "";
				String[] tempArr;
				while((line = br.readLine()) !=null) {
					tempArr = line.split(";");
					for (int i  = 1; i < tempArr.length - 1; i++) {
						dataNilai.add(Integer.valueOf(tempArr[i]));
					}
				}
				br.close();
				} catch(IOException ioe) {
					ioe.printStackTrace();
					System.out.println("-------------------------------------------------------------");
					System.out.println("Aplikasi Pengolah Nilai Siswa");
					System.out.println("-------------------------------------------------------------");
					System.out.println("File tidak ditemukan");
					System.out.println("Letakkan File csv dengan nama File data_sekolah di direktori!");
					System.out.println("0. Exit");
					System.out.println("-------------------------------------------------------------");

					Scanner input = new Scanner(System.in);
					int salah = input.nextInt();
					if (salah == 0) {
						System.exit(0);
					}
				}
				return dataNilai;

		}
		
		public static void hasil(Nilai_C3 nilai) throws IOException {
			String path = "C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_mean_median_modus.txt";
			File file = new File(path);	
			BufferedWriter hasil = new BufferedWriter(new FileWriter(file));

			hasil.write("Berikut Hasil Pengolahan Nilai : ");
			hasil.newLine();
			hasil.write(" ");
			hasil.newLine();
			hasil.write("Berikut hasil sebaran data nilai");
			hasil.newLine();
			hasil.write("Mean   : " + nilai.mean());
			hasil.newLine();
			hasil.write("Median : " + nilai.median());
			hasil.newLine();
			hasil.write("Modus  : " + nilai.modus());
			hasil.newLine();
			
			hasil.close();
			
		}
		
		public static void datahasil(Nilai_C3 nilai) throws IOException {
			String path = "C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_mean_median_modus.txt";
			File file = new File(path);	
			BufferedWriter datahasil = new BufferedWriter(new FileWriter(file));

			datahasil.write("Berikut Hasil Pengolahan Nilai : ");
			datahasil.newLine();
			datahasil.write(" ");
			datahasil.newLine();
			datahasil.write("Berikut hasil sebaran data nilai");
			datahasil.newLine();
			datahasil.write("Mean   : " + nilai.mean());
			datahasil.newLine();
			datahasil.write("Median : " + nilai.median());
			datahasil.newLine();
			datahasil.write("Modus  : " + nilai.modus());
			datahasil.newLine();
			
			datahasil.close();

		}
		
		public static void frek(Nilai_C3 nilai) throws IOException {
			String path = "C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_frekuensi.txt";
			File file = new File(path);	
			BufferedWriter tulis = new BufferedWriter(new FileWriter(file));

			tulis.write("Berikut Hasil Pengolahan Nilai : ");
			tulis.newLine();
			tulis.write(" ");
			tulis.newLine();
			tulis.write("    Nilai          |    Frekuensi");
			tulis.newLine();
			tulis.write("kurang dari 6      |    " + nilai.frekuensi(0,5));
			tulis.newLine();
			tulis.write("      6            |    " + nilai.frekuensi(6,6));
			tulis.newLine();
			tulis.write("      7            |    " + nilai.frekuensi(7,7));
			tulis.newLine();
			tulis.write("      8            |    " + nilai.frekuensi(8,8));
			tulis.newLine();
			tulis.write("      9            |    " + nilai.frekuensi(9,9));
			tulis.newLine();
			tulis.write("      10           |    " + nilai.frekuensi(10,10));
			tulis.newLine();
			
			tulis.close();
			}
		
		public static void menu(Nilai_C3 nilai) throws IOException {
		boolean ulang = true;
		while (true) {
		String pilih;
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println("Aplikasi Pengolah Nilai Siswa");
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
		System.out.println("berikut : C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3");
		System.out.println(" ");
		System.out.println("pilih menu :");
		System.out.println("1. Generate txt untuk menampilkan mean, median, modus");
		System.out.println("2. Generate txt untuk menampilkan frekuensi");
		System.out.println("3. Generate kedua file");
		System.out.println("0. Exit");
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.print("Masukan Pilihan Anda : ");
		
		Scanner m = new Scanner(System.in);
		String memilih = m.nextLine();
		switch(memilih) {
		case "1" :
			datahasil(nilai);
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("File telah di generate di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_sekolah_mean_median_modus.txt");
			System.out.println("silahkan cek");
			System.out.println(" ");
			System.out.println("1. Kembali ke menu utama");
			System.out.println("0. Exit");
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		    System.out.print("Masukan Pilihan Anda : ");
			pilih = m.nextLine();
		    		switch(pilih) {
		    		case "1" : 
		    			continue;
		    		case "0" :
		    			System.out.println("Terima Kasih Banyak!!");
		    		System.exit(0);
	 				default:
	 					System.out.println();
	 					System.out.println("Input yang Anda masukkan salah");	
	 		continue;
		    	}
		    		
		 case "2" :
			frek(nilai);
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("File telah di generate di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_sekolah_frekuensi.txt");
			System.out.println("silahkan cek");
			System.out.println(" ");
			System.out.println("1. Kembali ke menu utama");
			System.out.println("0. Exit");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.print("Masukan Pilihan Anda : ");	
			pilih = m.nextLine();
		    		switch(pilih) {
		    		case "1" : 
		    			continue;
		    		case "0" :
		    			System.out.println("Terima Kasih Banyak!!");
		    		System.exit(0);
	 				default:
	 					System.out.println();
	 					System.out.println("Input yang Anda masukkan salah");
	 		continue;
		    	}
		    		
		 case "3" :
			datahasil(nilai);
			frek(nilai);
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("File telah di generate di C:\\Users\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_sekolah_mean_median_modus.txt");
			System.out.println("File telah di generate di C:\\user\\ACER\\Documents\\binar\\student_cahyopratamas_binar\\Challenge\\src\\Challenge_3\\data_sekolah_frekuensi.txt");
			System.out.println("silahkan cek");
			System.out.println(" ");
			System.out.println("1. Kembali ke menu utama");
			System.out.println("0. Exit");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.print("Masukan Pilihan Anda : ");	
			pilih = m.nextLine();
			   		switch(pilih) {
			   		case "1" : 
			   			continue;
			   		case "0" :
			   			System.out.println("Terima Kasih Banyak!!");
			    	System.exit(0);
		 			default:
		 				System.out.println();
		 				System.out.println("Input yang Anda masukkan salah");
		 	continue;
			    }
		
		 case "0" :
			 System.out.println("Terima Kasih Banyak!!");
			 System.exit(0);
		 default:
			 	System.out.println();
				System.out.println("Input yang Anda masukkan salah");
		 continue;
		}
		}
	}
}