package Challenge_3;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Nilai_C3 {
	private ArrayList<Integer> nilai;
	Nilai_C3(ArrayList<Integer> nilai) {
		this.nilai = nilai;
	}

	public ArrayList<Integer> getNilai() {
		return nilai;
	}

	public void setNilai(ArrayList<Integer> nilai) {
		this.nilai = nilai;
	}
	public double mean() {
		double mean = nilai.stream().collect(Collectors.averagingDouble(s -> s));
//		float mean = 0;
//		for (int i = 0; i < nilai.size(); i++) {
//			mean += nilai.get(i) ;
//		}
//		return mean /nilai.size();
		return mean;
	}

	public float median() {
		float median = 0;
		if (nilai.size()%2 == 0) {
			median = (nilai.get(nilai.size()/2) + nilai.get(nilai.size()/2 - 1)) / 2;	
		} else {
			median = nilai.get(nilai.size()/2);
		}
			return median;
	}

	public int frekuensi(int min,int max) {
		int count = 0;
		for (int i = 0; i < nilai.size(); i++) {
			if(nilai.get(i) >=min && nilai.get(i) <=max) {
				count++;
			}
		}   	
		return count;
	}

	public int modus() {
		int modus = 0;
		int banyak = 0;
		for (int n : nilai) {
			int frekuensi = frekuensi(n,n);
			if (frekuensi > banyak) {
				modus = n;
				banyak = frekuensi;
			}
		}
			return modus;
	}

}