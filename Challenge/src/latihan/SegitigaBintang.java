package latihan;

import java.util.Scanner;

public class SegitigaBintang {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner masukan = new Scanner(System.in);
		
		System.out.print("Berapakah baris Segitiga yang diinginkan : ");
		int noOfRows = masukan.nextInt();
		int rowCount = 1;
		
		for (int i = noOfRows; i > 0; i++) {
			for(int j = 1; j <= i; j++) {
				System.out.print(" ");
			}
			for(int j = 1; j <= rowCount; j++) {
				System.out.print("* ");
			}
			System.out.println();
			rowCount++;
		}
		masukan.close();
	}

}
