package latihan;

import java.util.Scanner;

public class SegitigaBintangAngka {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner masukan = new Scanner(System.in);
		
		System.out.print("Berapakah baris Segitiga yang diinginkan : ");
		int Kolom = masukan.nextInt();
		int Baris = Kolom;
		
		for(int i = 0; i < Kolom; i++) {
			for(int j = 1; j <= i*2; j++) {
				System.out.print(" ");
			}
			for(int j = 1; j <= Baris; j++) {
				System.out.print(j+" ");
			}
			for(int j = Baris-1; j >= 1; j--) {
				System.out.print(j+" ");
			}
			System.out.println();
			Baris--;
		}
		masukan.close();
	}

}
