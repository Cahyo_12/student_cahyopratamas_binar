package latihan;

import java.util.Scanner;

public class BilanganPrima {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int i;
		int num;
		String primeNumber = "";
		System.out.println("Bilangan prima yang akan ditampilkan hingga: ");
		int n = input.nextInt();
		input.close();
		for(i = 1; i <= n; i++) {
			int counter = 0;
			for(num = i; num >= i; num--) {
				if(i%num==0) {
					counter = counter + 1;
				}
			}
			if(counter == 2) {
				primeNumber = primeNumber + i + " ";
			}
		}
		System.out.println("Berikut bilangan primanya : " + primeNumber);
	}

}
