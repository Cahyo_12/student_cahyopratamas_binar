package latihan;

import java.util.Scanner;

public class FizzBuzzMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		System.out.println("Enter Number:");
		int n = s.nextInt();
		System.out.println("The Number will be: ");
		for(int i = 1; i <= n; i++) {
			if(i % 3 == 0 && i % 5 == 0) {
				System.out.print("FizzBuzz");
			} else if (i % 3 == 0) {
				System.out.print("Fizz");
			} else if (i % 5 == 0) {
				System.out.print("Buzz");
			} else {
				System.out.print(i);
			}
			System.out.print(" ");
		}
		s.close();
	}

}