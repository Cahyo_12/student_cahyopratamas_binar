package latihan;

import java.util.Scanner;

public class Coba {

	public static int solve(int num) {
		// TODO Auto-generated method stub
		if (num == 1)
			return 1;
		else
			System.out.println(num + " * solve(" + (num - 1) + ")");
		return num * solve(num - 1);
	}
	
	public static void mai(String[] args) {
		Scanner input = new Scanner(System.in);
		int inputNum = 1;
		while(true) {
			inputNum = input.nextInt();
			if(inputNum < 0)
				break;
			System.out.println(" > " + solve(inputNum));
		}
		input.close();
	}
}
