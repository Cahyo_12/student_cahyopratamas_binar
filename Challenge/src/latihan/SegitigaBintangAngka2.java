package latihan;

import java.util.Scanner;

public class SegitigaBintangAngka2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner masukan = new Scanner(System.in);
		System.out.print("Berapakah baris Segitiga yang diinginkan : ");
		int n = masukan.nextInt();
		
		for(int i = 1; i < n; i++) {
			for(int j = 1; j <= i; j++) {
				System.out.print("[]");
			}
			System.out.println();
		}
		
		for(int i = n; i >= 1; i--) {
			for(int j = 1; j <= i; j++) {
				System.out.print("[]");
			}
			System.out.println();
		}
		masukan.close();
	}

}
