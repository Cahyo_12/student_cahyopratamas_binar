package latihan;

import java.util.Scanner;

public class DeretFibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner masukan = new Scanner(System.in);
		System.out.println("Berapa suku deret fibonacci? ");
		int n = masukan.nextInt();
		int firstTerm = 0;
		int secondTerm = 1;
		System.out.println("Deret fibonacci dengan " + n + " suku: ");
		
		for(int i = 1; i <= n; ++i) {
			System.out.print(firstTerm + ", ");
			
			int nextTerm = firstTerm + secondTerm;
			firstTerm = secondTerm;
			secondTerm = nextTerm;
		}
		masukan.close();
	}

}
