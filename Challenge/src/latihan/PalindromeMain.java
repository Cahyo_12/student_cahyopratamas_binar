package latihan;

import java.util.Scanner;

public class PalindromeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.print("Masukan String : ");
		
		String str = scanner.nextLine();
		String reverseStr = "";
		
		for(int i = str.length() - 1; i >= 0; i--) {
			reverseStr = reverseStr + str.charAt(i);
		}
		
		if(str.equals(reverseStr)) {
			System.out.println("String itu palindrome");
		} else {
			System.out.println("String itu bukan palindrome");
		}
		scanner.close();
	}

}
