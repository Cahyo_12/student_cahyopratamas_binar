package latihan;

import java.util.Scanner;

public class SegitigaPascal {

	public static void printPascal(int n) {
		// TODO Auto-generated method stub
		for (int line = 1; line <= n; line++) {
			for(int j = 0; j <= n; j++) {
				System.out.print(" ");
			}
			
			int c = 1;
			for(int i = 1; i <= line; i++) {
				
				System.out.print(c + " ");
				c = c * (line - 1) / i;
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Scanner masukan = new Scanner(System.in);
		System.out.println("Berapa tinggi segitiga yang diinginkan?");
		int n = masukan.nextInt();
		printPascal(n);
	}
}
