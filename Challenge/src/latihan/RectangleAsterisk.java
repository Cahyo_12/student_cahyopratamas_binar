package latihan;

import java.util.Scanner;

public class RectangleAsterisk {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner masukan = new Scanner(System.in);
		
		System.out.print("Panjang : ");
		int Panjang = masukan.nextInt();
		System.out.print("Lebar : ");
		int Lebar = masukan.nextInt();
		printRectangle(Panjang, Lebar);
	}
	static void printRectangle(int n, int m) {
		int i, j;
		for(i = 1; i <= n; i++ ) {
			for(j = 1; j <= m; j++) {
				if(i == 1 || i == n || j == 1 || j == m)
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.println();
		}
	}
}
